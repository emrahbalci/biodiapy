$('.hero-slider').owlCarousel({
	loop: true,
	// margin: 10,
	nav: false,
	autoPlay: 3000, //Set AutoPlay to 3 seconds
	dots: true,
	responsive: {
		0: {
			items: 1
		},
		600: {
			items: 1
		},
		1000: {
			items: 1
		}
	}
});

// Slick now

$('.product-slider-list').slick({
  dots: false,
  infinite: true,
	speed: 300,
	prevArrow: '<button type="button" class="slick-slider-prev"><span><img src="assets/images/left-arrow.svg"></span></button>',
	nextArrow: '<button type="button" class="slick-slider-next"><span><img src="assets/images/right-arrow.svg"></span></button>',
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.subscribe-product-slider-list').slick({
  dots: false,
  infinite: true,
	speed: 300,
	prevArrow: '<button type="button" class="slick-slider-prev"><span><img src="assets/images/left-arrow.svg"></span></button>',
	nextArrow: '<button type="button" class="slick-slider-next"><span><img src="assets/images/right-arrow.svg"></span></button>',
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.index-center-slider').slick({
  centerMode: true,
	centerPadding: '230px',
	prevArrow: '<div class="prev-button-col"><button type="button" class="slick-slider-prev"><span><img src="assets/images/left-arrow.svg"></span></button></div>',
	nextArrow: '<div class="next-button-col"><button type="button" class="slick-slider-next"><span><img src="assets/images/right-arrow.svg"></span></button></div>',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
				dots: false,
				centerPadding: '0'
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
				slidesToScroll: 1,
        centerPadding: '0'
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
				slidesToScroll: 1,
				centerPadding: '0'
      }
    }
  ]
});

$('.how-run-list-mobile').owlCarousel({
	loop: true,
	// margin: 10,
	nav: false,
	autoPlay: 3000, //Set AutoPlay to 3 seconds
	dots: true,
	responsive: {
		0: {
			items: 1,
			// stagePadding: 40
		},
		600: {
			items: 1
		},
		1000: {
			items: 1
		}
	}
});

$('.comments-slider').owlCarousel({
	loop: true,
	margin: 30,
	nav: true,
	dots: false,
	navText: ["<span><img src='assets/images/left-arrow.svg'></span>", "<span><img src='assets/images/right-arrow.svg'></span>"],
	responsive: {
		0: {
			items: 1,
			// stagePadding: 60
		},
		600: {
			items: 1,
			// stagePadding: 120
		},
		1000: {
			items: 3,
			// stagePadding: 230
		}
	}
});

$(function() {
  $('.menu-open').on('click', function() {
		$('body').addClass('header-mobile-active');
    $('.header-mobile-open').stop().slideDown();
    $('.menu-open').hide();
    $('.menu-close').show();
  })
  $('.menu-close').on('click', function() {
		$('body').removeClass('header-mobile-active');
    $('.header-mobile-open').stop().slideUp();
    $('.menu-open').show();
    $('.menu-close').hide();
  })
})

$(function() {
  $('.menu-button-bars').on('click', function() {
		$('body').addClass('header-mobile-active');
    $('.header-mobile').stop().slideDown();
    $('.menu-button-bars').hide();
    $('.menu-button-times').show();
  })
  $('.menu-button-times').on('click', function() {
		$('body').removeClass('header-mobile-active');
    $('.header-mobile').stop().slideUp();
    $('.menu-button-bars').show();
    $('.menu-button-times').hide();
  })
});

$('.left-area .control').on('mouseover touchstart', function (e) {
  e.preventDefault();
  $('.left-area .control').removeClass('active');
  $(this).addClass('active');
    var title = $(this).data('title');
    var desc = $(this).data('desc');
    var src = $(this).data('src');
    $('.right-area h2').html(title);
    $('.right-area p').html(desc);
    $('.right-area img').attr("src", src);
});


$(document).ready(function(){
    $(document).on('change', '.user-sidebar-select', function(){
        var val = $(this).val();
        window.location.replace(val);
    });

    $(document).on('click', 'button.basket-remove', function(e){
        e.preventDefault();

        var target = $(this).data('target');

        window.location.href = target;
    });
});

